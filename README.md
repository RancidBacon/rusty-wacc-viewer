## Rusty WACC Viewer

_Web Assembly Calling Card (WACC): Like an Avatar but WASM!_


 * Library:

    * `wacc`

 * Binaries:

    * `wacc-debug-dumper`

       Simple `wacc` crate usage example & WebAssembly Calling Card debug tool.

    * `wacc-viewer`

      Graphically renders a supplied WebAssembly Calling Card in a window.

    * `wacc-viewer-embedded`

      `embedded-graphics-simulator`-based WACC viewer/renderer.



See also: <https://wacc.rancidbacon.com/>


### Tool: `wacc-debug-dumper`

This example avoids more dependencies than any of the graphical
options but doesn't render the WACC.

It can be used to help debug a WACC `.wacc.wasm` file you've
created by checking that the frames output are what you expect.


#### Usage

```text
   wacc-debug-dumper [example.wacc.wasm]
```

If no filename is supplied one of the examples shipped with
the crate is used.

See the `samples/` directory for more sample WACC files to view,
such as `samples/logo.wacc.wasm`.



### Tool: `wacc-viewer`

Graphically renders a supplied WebAssembly Calling Card in a window.

*Does* support per-vertex colors.


#### Usage

```text
   wacc-viewer [example.wacc.wasm]
```

If no filename is supplied one of the sample files shipped with
this crate is used.

See the `samples/` directory for more sample WACC files to view,
such as `samples/heart.wacc.wasm`--which features per-vertex
colors!



### Tool: `wacc-viewer-embedded`

Graphically renders a supplied WebAssembly Calling Card in a window
provided by the [`embedded-graphics-simulator`](https://crates.io/crates/embedded-graphics-simulator) crate.

This is a first step toward a viewer that works on embedded hardware. (Shhh... :) )

Does not currently support per-vertex colors.


#### Usage

```text
   wacc-viewer-embedded [example.wacc.wasm]
```

If no filename is supplied one of the sample files shipped with
this crate is used.

See the `samples/` directory for more sample WACC files to view,
such as `samples/logo.wacc.wasm`.
