//!
//! # `wacc-viewer-embedded` -- `embedded-graphics-simulator`-based WACC viewer/renderer.
//!
//! Graphically renders a supplied WebAssembly Calling Card in a window
//! provided by the `embedded-graphics-simulator` crate.
//!
//! This is a first step toward a viewer that works on embedded hardware.
//!
//!
//! # Usage
//!
//! ```text
//! wacc-viewer-embedded [example.wacc.wasm]
//! ```
//!
//! If no filename is supplied one of the sample files shipped with
//! this crate is used.
//!
//! See the `samples/` directory for more sample WACC files to view,
//! such as `samples/logo.wacc.wasm`.
//!

use std::error::Error;

use ::wacc::*;

use embedded_graphics::{prelude::*, pixelcolor::{Rgb888, Rgb555}, primitives::Triangle, style::PrimitiveStyle};
use embedded_graphics_simulator::{OutputSettingsBuilder, SimulatorDisplay, Window, SimulatorEvent};

use std::thread;
use std::time::Duration;


use std::convert::Into;


/// Target display pixel dimensions: 480 x 272
const DISPLAY_WIDTH: u32 = 480;
const DISPLAY_HEIGHT: u32 = 272;

/// Simulator display scale factor for improved desktop visibility.
const DISPLAY_SCALE: u32 = 1;


fn setup_display(mut calling_card: CallingCard) -> Result<(), Box<dyn Error>> {

    // via <https://docs.rs/embedded-graphics-simulator/0.2.1/embedded_graphics_simulator/struct.SimulatorDisplay.html>
    let output_settings = OutputSettingsBuilder::new().scale(DISPLAY_SCALE).build();
    let mut display: SimulatorDisplay<Rgb888> = SimulatorDisplay::new(Size::new(DISPLAY_WIDTH, DISPLAY_HEIGHT));

    // via <https://docs.rs/embedded-graphics-simulator/0.2.1/embedded_graphics_simulator/#simulate-a-128x64-ssd1306-oled>:
    let mut window = Window::new("Simulator Display", &output_settings);

    display.clear(Rgb888::BLACK)?;
    window.update(&display);

    'mainloop: loop {

        for current_triangle in calling_card.next_frame().accumulated_triangles {

            // TODO: Handle alpha.
            // TODO: Handle per vertex color.
            let color1 = current_triangle.colors[0];
            let color1 = Rgb555::new(color1[0], color1[1], color1[2]);

            // See e.g.: <https://docs.rs/embedded-graphics/0.6.2/embedded_graphics/#draw-a-triangle>
            Triangle::new(
                current_triangle.coords[0].as_tuple().into(),
                current_triangle.coords[1].as_tuple().into(),
                current_triangle.coords[2].as_tuple().into(),
                )
            .into_styled(PrimitiveStyle::with_fill(Rgb888::from(color1)))
            .draw(&mut display)?;

        }

        window.update(&display);
        display.clear(Rgb888::BLACK)?;

        thread::sleep(Duration::from_millis(1000)); // TODO: Handle better.

        for event in window.events() {
            match event {
                SimulatorEvent::Quit => break 'mainloop,
                _ => {}
            }
        }

    };

    Ok(())
}



use std::env;

const DEFAULT_SAMPLE_WACC_FILE_PATH: &str = "./samples/00-one-triangle.wacc.wasm";

fn main() -> Result<(), Box<dyn Error>> {

    let module_file_path = env::args().nth(1).unwrap_or(DEFAULT_SAMPLE_WACC_FILE_PATH.to_string());
    println!("Loading: {:?}", module_file_path);

    // Attempt to load a WACC-compatible WASM file...
    let calling_card = CallingCard::from_file(&module_file_path)?;

    setup_display(calling_card)?;

    Ok(())
}
