use std::error::Error;

use ::wacc::*;

use eframe::{egui, epi};


struct WaccViewerApp {

    _resize_done: bool, // Hack, see below.

    calling_card: CallingCard,

    previous_time: f64,

    current_frame: WaccFrame,

}


impl WaccViewerApp {

    fn with_calling_card(calling_card: CallingCard) -> WaccViewerApp {
        WaccViewerApp {_resize_done: false, calling_card, previous_time: -1., current_frame: WaccFrame {accumulated_triangles: vec!()}}
    }

}


impl epi::App for WaccViewerApp {

    fn name(&self) -> &str {
        "WACC Viewer" // TODO: Add filename to window title?
    }


    // via <https://docs.rs/epi/0.8.0/epi/trait.App.html#method.is_resizable>
    fn is_resizable(&self) -> bool {
        true // *sigh* but seems it's broken in different ways with both `true` & `false`.
    }


    fn setup(&mut self, _ctx: &egui::CtxRef) {

    }


    fn update(&mut self, ctx: &egui::CtxRef, frame: &mut eframe::epi::Frame<'_>) {

        if (ctx.input().time-self.previous_time) >= 1. {
            self.previous_time = ctx.input().time;
            self.current_frame = self.calling_card.next_frame();
        }


        // TODO: Consider implementing as a widget?
        //       See: <https://github.com/emilk/egui/blob/f5431f308a58c084f700eb5417d52c02f45a0d5d/egui_demo_lib/src/apps/demo/toggle_switch.rs#L1>

        // TODO: Handle this better.
        //       e.g. see: <https://docs.rs/epaint/0.9.0/epaint/color/struct.Rgba.html#method.from_rgb>
        //                 <https://docs.rs/epaint/0.9.0/epaint/color/fn.linear_f32_from_linear_u8.html>
        //                 <https://docs.rs/epaint/0.9.0/epaint/color/fn.linear_u8_from_linear_f32.html>
        //                 `wacc::helper::color_f32_from_int()`
        fn convert_color_5551_to_8888(color5551: [u8; 4]) -> [u8; 4] {
            [
                ((color5551[0] as f32/31.)*255.) as u8,
                ((color5551[1] as f32/31.)*255.) as u8,
                ((color5551[2] as f32/31.)*255.) as u8,
                ((color5551[3] as f32/31.)*255.) as u8,
            ]
        }


        let mut tris = vec!();

        for current_triangle in &self.current_frame.accumulated_triangles {

            let mut tri = egui::paint::Mesh::default();

            // TODO: Handle alpha.

            let color0 = current_triangle.colors[0];
            let color0 = convert_color_5551_to_8888(color0);
            let color0 = egui::Color32::from_rgb(color0[0], color0[1], color0[2]);

            let color1 = current_triangle.colors[1];
            let color1 = convert_color_5551_to_8888(color1);
            let color1 = egui::Color32::from_rgb(color1[0], color1[1], color1[2]);

            let color2 = current_triangle.colors[2];
            let color2 = convert_color_5551_to_8888(color2);
            let color2 = egui::Color32::from_rgb(color2[0], color2[1], color2[2]);


            let (x0, y0) = current_triangle.coords[0].as_tuple();
            tri.colored_vertex(egui::pos2(x0 as f32, y0 as f32), color0);

            let (x1, y1) = current_triangle.coords[1].as_tuple();
            tri.colored_vertex(egui::pos2(x1 as f32, y1 as f32), color1);

            let (x2, y2) = current_triangle.coords[2].as_tuple();
            tri.colored_vertex(egui::pos2(x2 as f32, y2 as f32), color2);

            tri.add_triangle(0, 1, 2);

            tris.push(egui::Shape::Mesh(tri));
        }


        // via <https://github.com/emilk/egui/blob/37a3fe7f76c1e6c39e0affa7b4b038b77169465b/eframe/examples/hello_world.rs>
        // See also: <https://docs.rs/egui/0.8.0/egui/containers/struct.CentralPanel.html>
        egui::CentralPanel::default().show(ctx, |ui| {

            ui.set_max_size(egui::Vec2::new(256., 256.));

            // See: <https://docs.rs/egui/0.8.0/egui/struct.Painter.html#method.add>
            //      <https://docs.rs/egui/0.8.0/egui/struct.Painter.html#method.extend>
            ui.painter().extend(tris);
            ui.output().needs_repaint = true;

            ui.set_min_size(egui::Vec2::new(256., 256.));

            ui.shrink_height_to_current();
            ui.shrink_width_to_current();

            ui.set_clip_rect(egui::Rect::from_x_y_ranges(256.0..=256.0, 256.0..=256.0));

        });


        // Hack to work around issues with window resize/redraw.
        // This at least shrinks the window when first viewed.
        if !self._resize_done {
            // via <https://github.com/emilk/egui/blob/37a3fe7f76c1e6c39e0affa7b4b038b77169465b/eframe/examples/hello_world.rs>
            frame.set_window_size(egui::Vec2::new(256., 256.));
            self._resize_done = true;
        }



    }
}


use std::env;

const DEFAULT_SAMPLE_WACC_FILE_PATH: &str = "./samples/00-one-triangle.wacc.wasm";


fn main() -> Result<(), Box<dyn Error>> {

    let module_file_path = env::args().nth(1).unwrap_or(DEFAULT_SAMPLE_WACC_FILE_PATH.to_string());
    println!("Loading: {:?}", module_file_path);

    // Attempt to load a WACC-compatible WASM file...
    let calling_card = CallingCard::from_file(&module_file_path)?;

    // Boilerplate via:
    //
    //  * <https://github.com/emilk/egui/blob/37a3fe7f76c1e6c39e0affa7b4b038b77169465b/eframe/src/lib.rs#L54>
    //  * <https://github.com/emilk/egui/blob/37a3fe7f76c1e6c39e0affa7b4b038b77169465b/eframe/examples/hello_world.rs#L44>
    //  * <https://docs.rs/crate/eframe/0.8.0/source/src/lib.rs>
    //
    eframe::run_native(Box::new(WaccViewerApp::with_calling_card(calling_card)))

}
