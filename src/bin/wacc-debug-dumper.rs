//!
//! # `wacc-debug-dumper` -- Simple [`wacc`] usage example & debug tool.
//!
//! This example avoids more dependencies than any of the graphical
//! options but doesn't render the WACC.
//!
//! It can be used to help debug a WACC `.wacc.wasm` file you've
//! created by checking the frames output are what you expect.
//!
//!
//! # Usage
//!
//! ```text
//! wacc-debug-dumper [example.wacc.wasm]
//! ```
//!
//! If no filename is supplied one of the examples shipped with
//! the crate is used.
//!

use std::env;
use std::error::Error;

use wacc::CallingCard;


const DEFAULT_SAMPLE_WACC_FILE_PATH: &str = "./samples/00-one-triangle.wacc.wasm";

const MAX_FRAME_COUNT: u32 = 5; // TODO: Make this configurable.

fn main() -> Result<(), Box<dyn Error>> {

    let module_file_path = env::args().nth(1).unwrap_or(DEFAULT_SAMPLE_WACC_FILE_PATH.to_string());
    println!();
    println!("Loading: {:?}", module_file_path);

    // Attempt to load a WACC-compatible WASM file...
    let mut calling_card = CallingCard::from_file(&module_file_path)?;

    for i in 1..=MAX_FRAME_COUNT {

        // TODO: Improve default debug output in library.

        let current_frame = calling_card.next_frame();
        println!();
        println!("Frame #{:?}:", i);

        for current_triangle in current_frame.accumulated_triangles {

            println!();
            println!("  Triangle:");
            println!("    coords: {:?}", current_triangle.coords);
            println!("    colors: {:?}", current_triangle.colors);

        }

    }

    println!();

    Ok(())
}
